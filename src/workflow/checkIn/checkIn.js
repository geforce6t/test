import React from 'react'
import { Navbar } from '../../components/navbar'
import { Home } from '../../pages/home'
import { CreatePatient } from './pages/createPatient'
import { EditPatient } from './pages/editPatient'
import {
  component
} from '../../reducers/checkin/checkin'
import { useSelector } from 'react-redux'

export const CheckIn = () => {
  const activeComponent = useSelector(component)
  const components = activeComponent === 'CreatePatient' ? [Home, CreatePatient] : [Home, EditPatient]
  return (
    <Navbar labels={['HOME', 'CREATE / EDIT PATIENT']} component={components} />
  )
}

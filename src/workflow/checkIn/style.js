import { makeStyles } from '@material-ui/core/styles'

const shadow = '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)'

export const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(1),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  form: {
    backgroundColor: '#272727',
    letterSpacing: '2pt',
    margin: theme.spacing(6, 0, 6),
    padding: theme.spacing(6),
    boxShadow: shadow,
    borderRadius: '1%',
    [theme.breakpoints.down('sm')]: {
      width: '95%',
      backgroundColor: '#272727',
      padding: theme.spacing(3)
    }
  },
  resource: {
    margin: '2%'
  },
  root: {
    width: '100%'
  },
  container: {
    maxHeight: 440
  },
  submit: {
    margin: theme.spacing(2, 0, 4),
    boxShadow: shadow,
    width: '30%',
    backgroundColor: '#000000',
    [theme.breakpoints.down('sm')]: {
      width: '100%'
    }
  },
  account: {
    backgroundColor: '#272727',
    letterSpacing: '2pt',
    width: '20%',
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(6),
    padding: theme.spacing(5),
    boxShadow: shadow,
    textAlign: 'center',
    borderRadius: '1%',
    [theme.breakpoints.down('sm')]: {
      width: '95%',
      backgroundColor: '#272727',
      padding: theme.spacing(3)
    }
  },
  icon: {
    fontSize: '8rem'
  }
}))

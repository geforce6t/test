import React, { useEffect, useState, useRef } from 'react'
import {
  CssBaseline,
  Container,
  Grid,
  Typography,
  Button
} from '@material-ui/core'
import AccountCircleIcon from '@material-ui/icons/AccountCircle'
import { useStyles } from '../style'
import {
  patient,
  createapage
} from '../../../reducers/checkin/checkin'
import { useSelector, useDispatch } from 'react-redux'
import '@lh-toolkit/fhir-patient-get/fhir-patient-get.js'
import axios from 'axios'
import { config } from '../../../config'
import { Loading } from '../../../components/loading'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

export const EditPatient = () => {
  const classes = useStyles()
  const reference = useRef()
  const dispatch = useDispatch()

  const empytState = {
    resourceType: 'Patient',
    id: '',
    active: false,
    name: [{}],
    telecom: [{}],
    gender: '',
    birthDate: '',
    deceasedBoolean: true,
    language: [{}],
    address: [{}],
    identifier: [{}],
    contact: [{ relationship: [{ coding: [{}] }], name: { given: [] }, telecom: [{}], address: { line: [] } }]
  }

  const activePatient = useSelector(patient)
  const [loading, setLoading] = useState(true)
  const [nodeReady, setNodeReady] = useState(false)
  const [rootelem, setRootElem] = useState({})
  const [value, setValue] = useState(empytState)

  useEffect(() => {
    if (reference.current && nodeReady === false) {
      setNodeReady(true)
      setRootElem(reference.current)
    }
  })

  useEffect(() => {
    const fetchPatient = async () => {
      const res = await axios.get(`${config.basename}/Patient/${activePatient}`)

      setValue((prev) => {
        const relationship = res.data.contact?.[0]?.relationship || [{ coding: [{}] }]
        const name = res.data.contact?.[0]?.name || { given: [] }
        const telecom = res.data.contact?.[0]?.telecom || [{}]
        const address = res.data.contact?.[0]?.address || { line: [] }

        return {
          resourceType: prev.resourceType,
          id: res.data?.id || prev.id,
          active: res.data?.active || prev.active,
          name: res.data?.name || prev.name,
          telecom: res.data?.telecom || prev.telecom,
          gender: res.data?.gender || prev.gender,
          birthDate: res.data?.birthDate || prev.birthDate,
          deceasedBoolean: res.data?.deceasedBoolean || prev.deceasedBoolean,
          language: res.data?.language || prev.language,
          address: res.data?.address || prev.address,
          identifier: res.data?.identifier || prev.identifier,
          contact: [{ relationship, name, telecom, address }]
        }
      })

      setLoading(false)
    }
    fetchPatient()
  }, [])

  const EditResource = async () => {
    const res = await axios.put(`${config.basename}/Patient/${activePatient}`, rootelem.value)

    if (res.status === 200) {
      toast.dark('SUCCESSFULLY EDITED !')
    } else {
      toast.error('INTERNAL SERVER ERROR !')
    }
    dispatch(createapage())
  }

  return (
    loading
      ? (
        <Loading />)
      : (
    <Container component="main" maxWidth="lg">
      <CssBaseline />

      <div className={classes.paper}>
            <>
              <Grid
                className={classes.account}
                container
                direction="column"
                justify="center"
                alignItems="center"
              >
                <AccountCircleIcon className={classes.icon} color="primary" fontSize="large" />
              </Grid>

              <Button
                aria-label="submit"
                type="submit"
                variant="text"
                color="secondary"
                className={classes.submit}
                onClick={() => dispatch(createapage())}
              >
                BACK TO CREATE PATIENT
              </Button>

              <Typography component="h1" variant="h4">
                EDIT PATIENT
              </Typography>

              <Grid
                className={classes.form}
                container
                direction="column"
                justify="center"
                alignItems="center"
              >
                <fhir-patient-get ref={reference} patientMarriage='false' value={JSON.stringify(value)} />
                <Button
                  aria-label="submit"
                  type="submit"
                  variant="text"
                  color="secondary"
                  className={classes.submit}
                  onClick={EditResource}
                >
                  SAVE
                </Button>
              </Grid>
            </>
      </div>

    </Container>
        )
  )
}

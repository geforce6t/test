import React, { useRef } from 'react'
import {
  CssBaseline,
  Container,
  Grid,
  Typography
} from '@material-ui/core'
import { useStyles } from '../style'
import {
  editpage
} from '../../../reducers/checkin/checkin'
import { FhirSearch } from '../../../components/search'
import { config } from '../../../config'
import '@lh-toolkit/fhir-create-patient/fhir-create-patient.js'

export const CreatePatient = () => {
  const classes = useStyles()
  const reference = useRef()

  return (
    <Container component="main" maxWidth="lg">
      <CssBaseline />
      <div className={classes.paper}>
          <Typography component="h1" variant="h4">
            SEARCH PATIENT
          </Typography>
          <FhirSearch param={[
            {
              value: 'identifier',
              label: 'identifier'
            },
            {
              value: 'name',
              label: 'name'
            }
          ]}

          workflow="checkin"
          resource="Patient"
          dispatchaction={editpage}

          columns={[
            {
              id: 'id',
              label: 'ID'
            },
            {
              id: 'name',
              label: 'NAME'
            },
            {
              id: 'identifier',
              label: 'IDENTIFIER'
            },
            {
              id: 'active',
              label: 'ACTIVE STATUS'
            }
          ]}
          />
        <Typography component="h1" variant="h4">
          CREATE PATIENT
        </Typography>
        <Grid
          className={classes.form}
          container
          direction="column"
          justify="space-evenly"
          alignItems="flex-end"
        >
          <fhir-create-patient ref={reference} url ={`${config.basename}Patient/`}/>
        </Grid>
      </div>
    </Container>
  )
}

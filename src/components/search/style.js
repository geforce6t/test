import { makeStyles } from '@material-ui/core/styles'

const shadow = '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)'

export const useStyles = makeStyles((theme) => ({
  divider: {
    height: 40,
    margin: 4
  },
  paper: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  logo: {
    [theme.breakpoints.down('sm')]: {
      width: '95%'
    }
  },
  form: {
    backgroundColor: '#272727',
    letterSpacing: '2pt',
    width: '120%',
    marginTop: theme.spacing(3),
    padding: theme.spacing(6),
    boxShadow: shadow,
    textAlign: 'center',
    background: `url(${Image})`,
    borderRadius: '1%',
    [theme.breakpoints.down('sm')]: {
      width: '95%',
      backgroundColor: '#272727',
      padding: theme.spacing(3)
    }
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    boxShadow: shadow,
    width: '30%',
    backgroundColor: '#000000',
    [theme.breakpoints.down('sm')]: {
      width: '100%'
    }
  },
  input: {
    width: '40%',
    [theme.breakpoints.down('sm')]: {
      width: '100%'
    }
  },
  inputfields: {
    margin: theme.spacing(3, 0, 2)
  },
  row: {
    boxShadow: shadow
  },
  container: {
    boxShadow: shadow
  }
}))

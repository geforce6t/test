import React, { useState } from 'react'
import { useStyles } from './style'
import PropTypes from 'prop-types'
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Button,
  MenuItem,
  CssBaseline,
  Container,
  Grid,
  TableFooter,
  IconButton
} from '@material-ui/core'
import ArrowRightIcon from '@material-ui/icons/ArrowRight'
import ArrowLeftIcon from '@material-ui/icons/ArrowLeft'
import axios from 'axios'
import { ApplySearchParam } from '../../utils/functions'
import { useDispatch } from 'react-redux'
import { config } from '../../config'
import { Loading } from '../loading'

export const FhirSearch = ({ param, workflow, columns, resource, dispatchaction }) => {
  const classes = useStyles()
  const dispatch = useDispatch()
  const [searchParam, setSearchParam] = useState(param[0].value)
  const [value, setValue] = useState('')
  const [result, setResult] = useState([])
  const [loading, setLoading] = useState(false)
  const [previousPage, setPreviousPage] = useState('')
  const [nextPage, setNextPage] = useState('')

  const Paginate = (versionAr) => {
    if (versionAr.length === 1) {
      setPreviousPage('')
      setNextPage('')
    }

    if (versionAr.length === 3) {
      setPreviousPage(versionAr[2].url)
      setNextPage(versionAr[1].url)
    }

    if (versionAr.length === 2 && versionAr[1].relation === 'previous') {
      setNextPage('')
      setPreviousPage(versionAr[1].url)
    } else if (versionAr.length === 2 && versionAr[1].relation === 'next') {
      setNextPage(versionAr[1].url)
      setPreviousPage('')
    }
  }

  const search = (url) => {
    setLoading(true)
    const getBundle = async () => {
      const res = await axios.get(url)

      Paginate(res.data.link)
      const emptyValueCheck = res.data.entry || []
      // need to get a way to make the following method run from prop value
      const filteredValue = emptyValueCheck.map((item) => {
        return ApplySearchParam(item, workflow)
      })
      setResult(filteredValue)
      setLoading(false)
    }
    getBundle()
  }

  return (
    (loading
      ? <Loading />
      : <Container component="main" maxWidth="md">
      <CssBaseline />
      <div className={classes.paper}>
        <Grid
            className={classes.form}
            container
            justify="space-evenly"
          >
          <TextField
            className={classes.input}
            variant="filled"
            margin="none"
            value={value}
            onChange={(e) => setValue(e.target.value)}
            id="search"
            label="Search"
            name="search"
            autoComplete="search"
            autoFocus
          />
        <TextField
          id="search-param"
          className={classes.input}
          select
          variant="filled"
          label="Select"
          value={searchParam}
          onChange={(e) => setSearchParam(e.target.value)}
          helperText="Please select a seach parameter"
        >
          {param.map((option) => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
        </TextField>
        <Button
          aria-label="submit"
          type="submit"
          variant="text"
          color="secondary"
          className={classes.submit}
          onClick={() => search(`${config.basename}${resource}?${searchParam}=${value}`)}
        >
          SEARCH
        </Button>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow className={classes.row}>
                {columns.map((column, index) => (
                  <TableCell
                    key={`head${index}`}
                    align="center"
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {result.map((row, rowindex) => {
                return (
                  <TableRow className={classes.row} hover role="checkbox" tabIndex={-1} key={rowindex}>
                    {columns.map((column, colindex) => {
                      const value = row[column.id]
                      return (
                        /* this row.id will be used to send the users to the edit patient page components
                        */
                        <TableCell onClick={(e) => dispatch(dispatchaction(row.id))} align="center" size="small" key={`col${colindex}`}>
                          {value}
                        </TableCell>
                      )
                    })}
                  </TableRow>
                )
              })}
            </TableBody>
            <TableFooter>
              <IconButton onClick={() => previousPage !== '' ? search(previousPage) : null} aria-label="left-arrow">
                <ArrowLeftIcon />
              </IconButton>
              <IconButton onClick={() => nextPage !== '' ? search(nextPage) : null} aria-label="right-arrow">
                <ArrowRightIcon />
              </IconButton>
            </TableFooter>
          </Table>
        </TableContainer>
        </Grid>
      </div>
    </Container>
    )
  )
}

FhirSearch.propTypes = {
  param: PropTypes.array,
  workflow: PropTypes.string,
  resource: PropTypes.string,
  columns: PropTypes.array,
  dispatchaction: PropTypes.func
}

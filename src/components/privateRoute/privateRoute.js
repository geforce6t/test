import React from 'react'
import { Route, useHistory } from 'react-router-dom'
import PropTypes from 'prop-types'
import {
  authStatus,
  userRole
} from '../../reducers/auth/authSlice'
import { useSelector } from 'react-redux'

export const PrivateRoute = ({ component, role, ...options }) => {
  const history = useHistory()
  const authstatus = useSelector(authStatus)
  const userrole = useSelector(userRole)

  if (!authstatus || role !== userrole) {
    history.push('/login')
  } else {
    return <Route {...options} component={component} />
  }
  return null
}

PrivateRoute.propTypes = {
  component: PropTypes.any.isRequired,
  role: PropTypes.string
}

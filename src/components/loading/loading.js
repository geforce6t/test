/**
 * this component is used to show a loading circular progress for async requests
 */
import React from 'react'
import {
  CssBaseline,
  Container,
  Grid,
  CircularProgress
} from '@material-ui/core'
import { useStyles } from './style'

export const Loading = () => {
  const classes = useStyles()

  return (
    <Container component="main" maxWidth="lg">
      <CssBaseline />
      <div className={classes.paper}>
        <Grid
          className={classes.form}
          container
          direction="column"
          justify="center"
          alignItems="center"
        >
          <CircularProgress color="secondary" />
        </Grid>
      </div>
    </Container>
  )
}

import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  form: {
    backgroundColor: '#272727',
    letterSpacing: '2pt',
    width: '100%',
    marginTop: theme.spacing(3),
    padding: theme.spacing(6),
    textAlign: 'center',
    background: `url(${Image})`,
    borderRadius: '1%',
    [theme.breakpoints.down('sm')]: {
      width: '95%',
      backgroundColor: '#272727',
      padding: theme.spacing(3)
    }
  }
}))

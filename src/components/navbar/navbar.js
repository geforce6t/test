import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { useStyles } from './style'
import { AppBar, Tabs, Tab, Typography, Box, CssBaseline, Container } from '@material-ui/core'
import ExittoApp from '@material-ui/icons/ExitToApp'
import logo from '../../assets/images/logosm.png'
import {
  logout
} from '../../reducers/auth/authSlice'
import { useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function TabPanel (props) {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={4}>
          {children}
        </Box>
      )}
    </div>
  )
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
}

export const Navbar = ({ labels, component }) => {
  const classes = useStyles()
  const [value, setValue] = useState(0)
  const dispatch = useDispatch()
  const history = useHistory()

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  return (
    <Container component="main" maxWidth="lg">
      <CssBaseline />
      <div className={classes.paper}>

        <div>
          <img src={logo} alt='logo'/>
        </div>

        <div className={classes.exit}>
          <button onClick={() => {
            dispatch(logout())
            toast.dark('LOGGED OUT !')
            history.push('/')
          }} className={classes.logoutbtn}
          >
            <ExittoApp />
          </button>
        </div>

        <AppBar position="static" className={classes.navbar}>
          <Tabs value={value} onChange={handleChange} aria-label="checkIn tabs">
            {labels.map((item) => <Tab key={item} label={<Typography variant="subtitle1">{item}</Typography>} />)}
          </Tabs>
        </AppBar>

        {component.map((Component, index) => {
          return <TabPanel key={index} value={value} index={index}><Component key={index} /></TabPanel>
        })}
      </div>
    </Container>
  )
}

Navbar.propTypes = {
  labels: PropTypes.array,
  component: PropTypes.array
}

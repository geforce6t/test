import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  navbar: {
    color: theme.palette.primary.main,
    backgroundColor: '#212121'
  },
  exit: {
    width: '100%',
    textAlign: 'end',
    margin: theme.spacing(1, 0, 1)
  },
  logoutbtn: {
    background: 'white',
    boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
    border: 'none'
  }
}))

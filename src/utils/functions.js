/* the following function is used in the search component,
this is the code that runs inside the map method of array results,
*/
export const ApplySearchParam = (item, workflow) => {
  switch (workflow) {
    case 'checkin':
      return {
        id: item.resource.id || ' ',
        identifier: item.resource.identifier?.[0]?.value || ' ',
        name: item.resource.name?.[0]?.family || item.resource.name?.[0]?.given?.[0] || ' ',
        active: +item.resource.active || ' '
      }
  }
}

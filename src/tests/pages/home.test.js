import React from 'react'
import { render } from '@testing-library/react'
import { Home } from '../../pages/home'
import { Provider } from 'react-redux'
import store from '../../redux/store'

test('Home component renders welcome text', () => {
  const { getByText } = render(<Provider store={store}><Home /></Provider>)
  // the admin text confirms that the redux selector is working properly
  expect(getByText('WELCOME admin !')).not.toBeNull()
})

import React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import { Auth } from '../../pages/auth'
import { Provider } from 'react-redux'
import store from '../../redux/store'
import { ToastContainer } from 'react-toastify'
import { BrowserRouter as Router } from 'react-router-dom'

test('allows the user to login successfully', async () => {
  render(<Provider store={store}><ToastContainer/><Router><Auth /></Router></Provider>)

  // fill out the form
  fireEvent.change(screen.getByLabelText(/username/i), {
    target: { value: 'benteke_cristian' }
  })
  fireEvent.change(screen.getByLabelText(/Password/), {
    target: { value: '02021994Va' }
  })

  fireEvent.click(screen.getByRole('button', {
    name: /submit/i
  }))

  // expect the toast text value for successful entry
  expect(await screen.findByText(/LOGGED IN !/)).toBeInTheDocument()
})

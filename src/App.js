import React from 'react'
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'
import { ThemeProvider } from '@material-ui/core/styles'
import { theme } from './theme'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { Auth } from './pages/auth'
import { PrivateRoute } from './components/privateRoute'
import { CheckIn } from './workflow/checkIn'

export const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <ToastContainer
        position="bottom-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      <Router basename={process.env.PUBLIC_URL}>
        <Switch>
            <Redirect exact from="/" to="/login" />
            <Route exact path="/login" component={Auth} />
            <PrivateRoute exact path="/checkIn" role="receptionist" component={CheckIn} />
        </Switch>
      </Router>
    </ThemeProvider>
  )
}

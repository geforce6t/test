import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'

import {
  Button,
  CssBaseline,
  TextField,
  Typography,
  Container,
  Grid,
  Divider,
  InputAdornment,
  IconButton
} from '@material-ui/core'

import Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'

import { useStyles } from './style'
import logo from '../../assets/images/logo.png'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

import { users } from '../../utils/credentials'
import {
  login,
  authStatus,
  activePath
} from '../../reducers/auth/authSlice'
import { useSelector, useDispatch } from 'react-redux'

export const Auth = () => {
  const classes = useStyles()
  const dispatch = useDispatch()
  const authstatus = useSelector(authStatus)
  const activepath = useSelector(activePath)
  const history = useHistory()

  // state used for toggling the view of the password textfield
  const [showPassword, setShowPassword] = useState(false)

  // state used to set the username
  const [username, setUsername] = useState('')

  // state used to set the password
  const [password, setPassword] = useState('')

  // this login function is only for demo purpose right now
  const Login = () => {
    // this flag is used to set the login status
    let loginStatus = false

    // this object will store the credentials of the user to be dispatched for the login action
    let usercred = {}

    // this method will check if the entered password and username match any entry in the mock credentials file
    users.forEach((user) => {
      if (user.username === username.trim() && user.password === password.trim()) {
        loginStatus = true
        usercred = user
      }
    })

    if (loginStatus) {
      toast.dark('LOGGED IN !')
      dispatch(login(usercred))
      history.push(usercred.path)
    } else {
      toast.error('INVALID CREDENTIALS !')
    }
  }

  useEffect(() => {
    if (authstatus) {
      history.push(activepath)
    }
  }, [])

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>

        <div className={classes.logo}>
          <img className={classes.logo} src={logo} alt='logo'/>
        </div>

        <Grid
          className={classes.form}
          container
          spacing={5}
          direction="column"
          justify="center"
          alignItems="center"
        >

          <Typography component="h1" variant="h4">
            LOGIN
          </Typography>

          <Divider light />

          <Grid
            className={classes.inputfields}
            container
            direction="column"
            justify="space-evenly"
            alignItems="flex-end"
          >

            <TextField
              className={classes.input}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              value={username}
              onChange={(e) => setUsername(e.target.value)}
              id="username"
              label="Username"
              name="username"
              autoComplete="name"
              autoFocus
            />

            <TextField
              className={classes.input}
              variant="outlined"
              margin="normal"
              required
              type={showPassword ? 'text' : 'password'}
              fullWidth
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              name="password"
              label="Password"
              id="password"
              autoComplete="current-password"
              InputProps={{ // <-- This is where the toggle button is added.
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={() => setShowPassword(!showPassword)}
                      onMouseDown={() => setShowPassword(!showPassword)}
                    >
                      {showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                )
              }}
            />

          </Grid>
          <Divider light />

          <Button
            aria-label="submit"
            type="submit"
            variant="text"
            color="secondary"
            className={classes.submit}
            onClick={Login}
          >
            LOGIN
          </Button>
        </Grid>
      </div>
    </Container>
  )
}

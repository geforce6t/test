import React from 'react'

import {
  CssBaseline,
  Typography,
  Container,
  Divider
} from '@material-ui/core'

import { useStyles } from './style'

import {
  userName
} from '../../reducers/auth/authSlice'

import { useSelector } from 'react-redux'

export const Home = () => {
  const classes = useStyles()
  const username = useSelector(userName)

  return (
    <Container component="main" maxWidth="md">
      <CssBaseline />
      <div className={classes.paper}>
          <Typography component="h1" variant="h2">
            WELCOME {username} !
          </Typography>
          <Divider light />
      </div>
    </Container>
  )
}

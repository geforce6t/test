import { createMuiTheme } from '@material-ui/core/styles'

export const theme = createMuiTheme({
  overrides: {
    MuiCssBaseline: {
      '@global': {
        '*': {
          'scrollbar-width': 'thin'
        },
        '*::-webkit-scrollbar': {
          width: '6px',
          height: '6px'
        },
        '*::-webkit-scrollbar-thumb': {
          background: 'white'
        }
      }
    }
  },
  palette: {
    type: 'dark',
    primary: {
      main: '#F2994A',
      dark: '#af6727',
      light: '#ffb06a'
    },
    secondary: {
      main: '#00B3A6',
      dark: '#019592',
      light: '#70EFDE'
    },
    text: {
      primary: '#fff',
      secondary: 'rbga(0, 0, 0, 0.85)'
    }
  },
  typography: {
    fontFamily: ['Roboto', 'Helvetica', 'Arial'].join(',')
  }
})

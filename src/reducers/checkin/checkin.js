import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  component: 'CreatePatient',
  patient: ' '
}

export const checkinSlice = createSlice({
  name: 'checkin',
  initialState,
  reducers: {
    createapage: (state) => {
      state.component = 'CreatePatient'
      state.patient = initialState.patient
    },
    editpage: (state, action) => {
      state.component = 'EditPatient'
      state.patient = action.payload
    }
  }
})

export const { createapage, editpage } = checkinSlice.actions
export const component = (state) => state.checkin.component
export const patient = (state) => state.checkin.patient

export default checkinSlice.reducer

import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  name: 'admin',
  role: 'admin',
  status: false,
  path: '/'
}

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    login: (state, action) => {
      state.name = action.payload.name
      state.role = action.payload.role
      state.status = true
      state.path = '/checkIn'
    },
    logout: (state) => {
      state.name = 'admin'
      state.role = 'admin'
      state.status = false
      state.path = '/'
    }
  }
})

export const { login, logout } = authSlice.actions
export const authStatus = (state) => state.auth.status
export const userName = (state) => state.auth.name
export const userRole = (state) => state.auth.role
export const activePath = (state) => state.auth.path

export default authSlice.reducer

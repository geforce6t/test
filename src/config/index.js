// for testing purposes the following object is not gitignored, else it should be hidden in production
export const config = {
  basename: 'http://hapi.fhir.org/baseR4/'
}
